package br.com.frachdev

import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.jetbrains.exposed.dao.*
import org.jetbrains.exposed.sql.Column
import java.util.*

class PlayerRank(id: EntityID<UUID>): UUIDEntity(id) {

    companion object : UUIDEntityClass<PlayerRank>(PlayerSchema)

    var name by PlayerSchema.name
    var rankId by PlayerSchema.rank

    var rank: Rank
        get() = Rank.ranks.filter { it.id == rankId }.first()
        set(value) { rankId = value.id }

    val player: Player
        get() = Bukkit.getPlayer(id.value)

}

object PlayerSchema: IdTable<UUID>(name = "ranks"){
    override val id = uuid("uuid").primaryKey().entityId()
    val name = varchar("name", length = 36)
    val rank = integer("rank")
}