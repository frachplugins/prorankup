package br.com.frachdev.inventories

import br.com.frachdev.Base
import br.com.frachdev.PlayerRank
import br.com.frachdev.Rank
import br.com.frachdev.RankAPI
import br.com.frachdev.utils.ItemStackBuilder
import br.com.frachdev.utils.inventory.ClickableItem
import br.com.frachdev.utils.inventory.SmartInventory
import br.com.frachdev.utils.inventory.content.InventoryContents
import br.com.frachdev.utils.inventory.content.InventoryProvider
import br.com.frachdev.utils.inventory.content.SlotPos
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.Sound
import org.bukkit.command.CommandException
import org.bukkit.entity.Player
import org.jetbrains.exposed.sql.transactions.transaction

class RankupInventory: InventoryProvider {

    companion object {
        fun open(player: Player){
            SmartInventory.builder().title("${ChatColor.DARK_GRAY}Menu de evolucao").size(3, 9).provider(RankupInventory()).build().open(player)
        }
    }

    override fun init(player: Player, contents: InventoryContents) {
        this.enable(player, contents)
    }

    override fun update(player: Player, contents: InventoryContents) {
        val state = contents.property("state", 0)
        contents.setProperty("state", state + 1)
        if (state % 5 != 0)
            return
        this.enable(player, contents)
    }

    private fun enable(player: Player, contents: InventoryContents){

        val playerRank = RankAPI.getRank(player)
        val nextRank: Rank? = playerRank!!.rank.getNext()

        contents.set(SlotPos(1, 2), ClickableItem.of(ItemStackBuilder(Material.STAINED_GLASS_PANE).setDurability(5).setName("${ChatColor.GREEN}Evoluir")
                .addLore("", "${ChatColor.GRAY}Clique para avancar de rank.", "")
                .build(), {
                    if(Base.economy.getBalance(player) < nextRank!!.price){
                        player.sendMessage("${ChatColor.RED}Ocorreu um erro, voc enao tem dinheiro suficiente para passar de rank");
                    } else {
                        player.sendMessage("${ChatColor.GREEN}EBAAAH! Voce passou para o proximo rank, parabens.")

                        player.closeInventory()
                        transaction(Base.dataSource) {
                            playerRank.rank = nextRank
                        }

                    }
        }))

        contents.set(SlotPos(1, 4), ClickableItem.of(
                nextRank!!.icon, {

                player.playSound(player.location, Sound.CAT_MEOW, 0.2F, 0.3F)

            }
        ))

        contents.set(SlotPos(1, 6), ClickableItem.of(
                ItemStackBuilder(Material.STAINED_GLASS_PANE).setDurability(14).setName("${ChatColor.RED}Cancelar").build(), {

                player.closeInventory()


        }
        ))

    }

}