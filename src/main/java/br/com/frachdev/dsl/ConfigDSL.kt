package br.com.frachdev.dsl;

import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.enchantments.Enchantment
import org.bukkit.inventory.ItemFlag
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.ItemMeta

fun ConfigurationSection.toItemStack(data: String): ItemStack{

    var stack: ItemStack

    val id = this.getInt("$data.Id")
    val dataId = this.getInt("$data.Data").toShort()
    val display = this.getString("$data.DisplayName")
    val loreList = this.getStringList("$data.Lore")
    val enchantList = this.getStringList("$data.Enchants")
    val flagList = this.getStringList("$data.ItemFlags")

    stack = ItemStack(Material.getMaterial(id)).apply {
        durability = dataId
        meta<ItemMeta>{
            displayName = ChatColor.translateAlternateColorCodes('&', display)
            lore = loreList
            for(ench in enchantList){
                addUnsafeEnchantment(Enchantment.getByName(ench.split(":")[0]), ench.split(":")[1] as Int)
            }
            for(flag in flagList){
                addItemFlags(ItemFlag.valueOf(flag))
            }
        }
    }

    return stack

}