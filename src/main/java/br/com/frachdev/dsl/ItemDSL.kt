package br.com.frachdev.dsl

import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.ItemMeta

inline fun <T : ItemMeta> ItemStack.meta(block: T.() -> Unit) = apply {
    itemMeta = (itemMeta as? T)?.apply(block) ?: itemMeta
}