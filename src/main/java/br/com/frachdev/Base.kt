package br.com.frachdev

import br.com.frachdev.utils.inventory.InventoryManager
import net.milkbowl.vault.economy.Economy
import org.bukkit.plugin.java.JavaPlugin
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import org.bukkit.plugin.RegisteredServiceProvider



class Base: JavaPlugin() {

    companion object {
        lateinit var dataSource: Database
            private set
        lateinit var inventoryManager: InventoryManager
        lateinit var economy: Economy
    }

    override fun onEnable() {
        init()
    }

    private fun init(){
        saveDefaultConfig()

        for(section: String in config.getConfigurationSection("Ranks").getKeys(false)) {
            Rank(config.getConfigurationSection("Ranks.$section"))
        }
        server.pluginManager.registerEvents(Eventos(), this)

        val host = config.getString("SQL.Host")
        val database = config.getString("SQL.Database")
        val username = config.getString("SQL.Username")
        val password = config.getString("SQL.Password")

        dataSource = Database.connect("jdbc:mysql://$host/$database", driver = "com.mysql.jdbc.Driver", user = "$username", password = "$password")
        transaction {
            SchemaUtils.create(PlayerSchema)
        }

        if(setupEconomy()){
            println("Vault e plugin de economia dectado!")
        } else {
            println("Nenhum pl de economia detectado!")
        }

        getCommand("rankup").executor = RankupCommand()

        inventoryManager = InventoryManager(this)
        inventoryManager.init()

    }

    private fun setupEconomy(): Boolean {
        if (server.pluginManager.getPlugin("Vault") == null) {
            return false
        }
        val rsp = server.servicesManager.getRegistration(Economy::class.java) ?: return false
        economy = rsp.provider
        return economy != null
    }


}