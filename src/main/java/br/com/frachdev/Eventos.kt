package br.com.frachdev

import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.jetbrains.exposed.sql.transactions.transaction

class Eventos: Listener{

    @EventHandler
    fun join(e: PlayerJoinEvent) {
        val player = e.player
        val playerRank: PlayerRank = transaction(Base.dataSource) {
            return@transaction PlayerRank.findById(player.uniqueId) ?: PlayerRank.new(player.uniqueId) {
                name = player.name
                rankId = Rank.ranks.first.id
            }
        }
        Rank.playerRanks.add(playerRank)
    }

    @EventHandler
    fun quit(e: PlayerQuitEvent){
        val player = e.player

        Rank.playerRanks.removeIf { it.player == player }

    }

}