package br.com.frachdev

import org.bukkit.entity.Player

class RankAPI {

    companion object {
        fun getRank(player: Player): PlayerRank? {
            return Rank.playerRanks.filter { it.id.value == player.uniqueId }.firstOrNull()
        }
    }

}