package br.com.frachdev

import br.com.frachdev.inventories.RankupInventory
import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandException
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class RankupCommand: CommandExecutor {

    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        if(sender is Player){
            if(args.size > 1) {
                if(args[0].equals("list", ignoreCase = true)){
                    sender.sendMessage("§aAbrindo menu contendo todas evoluções.")
                }
                return true
            }
            val playerRank = Rank.playerRanks.filter { it.id.value == sender.uniqueId }.firstOrNull()

            if(playerRank == null){
                sender.sendMessage("${ChatColor.RED}Nao foi possivel executar o comando, relogue no servidor ou contate um administrador")
                sender.closeInventory()
                return true
            }
            val nextRank: Rank? = playerRank.rank.getNext()
            if(nextRank == null){
                sender.sendMessage("${ChatColor.RED}Nao foi possivel evoluir, voce ja esta no ultimo rank")
                sender.closeInventory()
                return true
            }
            RankupInventory.open(sender)
        } else {
            sender.sendMessage("§cApenas players podem executar este comando.")
        }
        return false
    }

}