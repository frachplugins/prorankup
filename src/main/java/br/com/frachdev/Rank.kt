package br.com.frachdev

import br.com.frachdev.dsl.toItemStack
import org.bukkit.configuration.ConfigurationSection
import org.bukkit.inventory.ItemStack
import java.util.*
import kotlin.collections.HashSet

class Rank(rankName: ConfigurationSection){

    companion object {
        val ranks = LinkedList<Rank>()
        val playerRanks = HashSet<PlayerRank>()
    }

    var name = rankName.name
    var comandos: List<String> = rankName.getStringList("comandos")
    var price: Double = rankName.getDouble("preco")
    var icon: ItemStack = rankName.toItemStack("icon")

    var id: Int = rankName.getInt("position")

    init {
        ranks.add(this)
    }

    fun getNext(): Rank? {
        return ranks.filter { it.id == id+1 }.firstOrNull()
    }

}