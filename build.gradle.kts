import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.gradle.api.tasks.bundling.Jar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.10"
    id("com.github.johnrengelman.shadow") version "2.0.3"
    id("net.minecrell.plugin-yml.bukkit") version "0.2.1"
}

group = "br.com.frachdev"
version = "1.0-SNAPSHOT"

repositories {
    jcenter()
    mavenLocal()

    // spigot
    maven {
        name = "spigot"
        url = uri("https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
    }
    maven {
        name = "sonatype"
        url = uri("https://oss.sonatype.org/content/repositories/snapshots/")
    }

    maven {
        name = "vault"
        url = uri("http://nexus.hc.to/content/repositories/pub_releases")
    }

    // exposed
    maven {
        name = "exposed"
        url = uri("https://dl.bintray.com/kotlin/exposed")
    }
}

dependencies {
    compile(kotlin("stdlib"))
    compile("org.jetbrains.exposed:exposed:0.11.2")

    // dependecias nao compiladas dentro
    compileOnly(files(File(projectDir, "libs/spigot-1.8.8.jar")))
    compileOnly(files(File(projectDir, "libs/Vault.jar")))
}

tasks {
    "compileKotlin"(KotlinCompile::class) {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }
    "shadowJar"(ShadowJar::class) {
        baseName = project.name
        classifier = ""
    }
}

// plugin.yml
bukkit {
    name = "FrachProRankup"
    version = project.version.toString()
    main = "br.com.frachdev.Base"

    commands {
        "rankup" {
            description = "Comando simples de rankup"
        }
    }

    authors = listOf("FrachDev")
}